
# RELEASE    - Used as a basis for file names and downloads
# INSTALLDIR - the directory where the software is installed system wide
# SERVICEDIR - the directory where a site stores its data

RELEASE    = moin-1.9.9
INSTALLDIR = /opt
SERVICEDIR = /srv/wiki.fsfe.org
SERVICEUSR = www-data:www-data
include config.mk

PROGRAMS = gpg wget tar mv cp mkdir patch
SDIRLAYOUT = $(addprefix ${SERVICEDIR}/, static/ data/pages/ underlay/pages/ underlay/plugin/ \
                                         data/dict data/plugin static/fsfe \
                                         underlay/plugin/theme/fsfe.py \
                                         underlay/plugin/macro/EventCalendar.py \
                                         underlay/plugin/parser/Gallery2.py \
                                         moin.wsgi wikiconfig.py)

.PHONY: default install service

default: ${RELEASE}/
service: ${SDIRLAYOUT} ${INSTALLDIR}/${RELEASE}/
test: service wikiserver.py ${SERVICEDIR}/data/pages/

${RELEASE}.tar.gz.asc:
	wget -c http://static.moinmo.in/files/${@}

${RELEASE}.tar.gz: ${RELEASE}.tar.gz.asc
	wget -c -O '${@}.unverified' 'http://static.moinmo.in/files/${@}'
	gpg --homedir gnupg --verify '${<}' '${@}.unverified' \
	&& mv '${@}.unverified' '${@}'

${RELEASE}/: ${RELEASE}.tar.gz
	tar -xzf '${<}'
	touch '$@'

wikiserver.py: Makefile
	printf "\
	#!/usr/bin/env python\n\
	import sys, os\n\
	sys.path.insert(0, '%s')\n\
	moinpath = '%s'\n\
	sys.path.insert(0, moinpath)\n\
	os.chdir(moinpath)\n\
	from MoinMoin.script import MoinScript\n\
	if __name__ == '__main__':\n\
	    sys.argv = ['moin.py', 'server', 'standalone', '--docs', '%s']\n\
	    MoinScript().run()\n" \
	'$(abspath ${INSTALLDIR}/${RELEASE})' '$(abspath ${SERVICEDIR})' \
	'$(abspath ${SERVICEDIR}/static)' >'$@'
	chmod a+x '$@'

${SERVICEDIR}/moin.wsgi: Makefile ${SERVICEDIR}/
	printf "\
	#!/usr/bin/env python\n\
	import sys, os\n\
	sys.path.insert(0, '%s')\n\
	sys.path.insert(0, '%s')\n\
	from MoinMoin.web.serving import make_application\n\
	application = make_application(shared='%s')\n" \
	'$(abspath ${INSTALLDIR}/${RELEASE})' '$(abspath ${SERVICEDIR})' \
	'$(abspath ${SERVICEDIR}/static)' >'$@'
	chmod a+x '$@'

${SERVICEDIR}/wikiconfig.py: wikiconfig.py ${SERVICEDIR}/
	cp '$<' '$@'

${SERVICEDIR}/data/plugin: ${SERVICEDIR}/underlay/plugin/
	ln -srnf '$(abspath $<)' '$@'

${SERVICEDIR}/data/dict: ${SERVICEDIR}/data/
	-ln -snf /usr/share/dict '$@'

${SERVICEDIR}/static/fsfe: ${INSTALLDIR}/mointheme_fsfe/ ${SERVICEDIR}/static/
	ln -snf '$(abspath $<)' '$@'

${SERVICEDIR}/underlay/plugin/: ${INSTALLDIR}/${RELEASE}/ ${SERVICEDIR}/underlay/
	cp -Tau '$<wiki/data/plugin' '$@'

config.mk:
	printf "\
	INSTALLDIR = ${INSTALLDIR}\n\
	SERVICEDIR = ${SERVICEDIR}\n\
	SERVICEUSR = ${SERVICEUSR}\n" \
	>"$@"

define cp-plugin =
mkdir -p '$(dir $@)'
-chown '${SERVICEUSR}' '$(dir $@)'
cp -Tau '$<' '$@'
chmod a+x '$@'
endef

${SERVICEDIR}/data/pages/: example_pages/
	${cp-plugin}

text_x_gallery2-1.6.0-19.py:
	wget -O '$@' 'https://moinmo.in/ParserMarket/Gallery2?action=AttachFile&do=get&target=text_x_gallery2-1.6.0-19.py'
${SERVICEDIR}/underlay/plugin/parser/Gallery2.py: text_x_gallery2-1.6.0-19.py Gallery2.patch
	-patch -N <Gallery2.patch
	${cp-plugin}

EventCalendar-099b.py:
	wget -O '$@' 'https://moinmo.in/MacroMarket/EventCalendar?action=AttachFile&do=get&target=EventCalendar-099b.py'
${SERVICEDIR}/underlay/plugin/macro/EventCalendar.py: EventCalendar-099b.py
	${cp-plugin}

${SERVICEDIR}/underlay/plugin/theme/fsfe.py: mointheme_fsfe.py
	${cp-plugin}

${SERVICEDIR}/:
	mkdir -p '$@'

${SERVICEDIR}/%/:
	mkdir -p '$@'
	-chown ${SERVICEUSR} '$@'

${INSTALLDIR}/%/: %/
	mkdir -p '${@D}'
	cp -Tau '$<' '$@'
